﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Unit : MonoBehaviour
{

    public string unitName;
    public float unitLevel;

    public float atk;
    public float def;
    public float spd;

    public float maxHP;
    public float currentHP;

    public bool TakeDamage(float atk, float unitLevel, float def)
    {
        currentHP -= ((((((2 * unitLevel)/5) + 2) * 90 * atk/def) / 50) + 2) * Random.Range(.8f, 1.0f);

        if (currentHP <= 0)
            return true;
        else
            return false;
    }

    public bool MagicDamage(float atk, float unitLevel, float def)
    {
        currentHP -= ((((((2 * unitLevel) / 5) + 2) * 30 * atk / def) / 50) + 2) * Random.Range(.8f, 1.0f);

        if (currentHP <= 0)
            return true;
        else
            return false;
    }

    public void Heal(float amount)
    {
        //Increases Current HP
        currentHP += (amount * Random.Range(.2f, .5f));
        if (currentHP > maxHP)
            currentHP = maxHP;
    }

}
