﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public enum BattleState { START, PLAYERTURN, ENEMYTURN, WON, LOST}
public class BattleSystem : MonoBehaviour
{

    public GameObject playerPrefab;
    public GameObject enemyPrefab;
    public GameObject enemyPrefab2;
    public GameObject enemyPrefab3;
    public GameObject finalBoss;
    public GameObject[] enemyType = new GameObject[3];
    public Transform[] enemyBattleStation = new Transform[5];
    public GameObject atkButton1;
    public GameObject atkButton2;
    public GameObject atkButton3;
    public GameObject atkButton4;
    public GameObject atkButton5;
    public GameObject healButton;
    public GameObject magicButton;
    public GameObject newGame;

    public Animator enemyAnim;

    public Transform playerBattleStation;
    public Transform EBS1;
    public Transform EBS2;
    public Transform EBS3;
    public Transform EBS4;
    public Transform EBS5;

    public int type;
    public int spawn;
    public int count = 0;
    public int battleCount = 0;
    public int magCount = 0;
    public int winCount = 0;

    Unit playerUnit;
    Unit enemyUnit;
    Unit enemyUnit2;
    Unit enemyUnit3;
    Unit enemyUnit4;
    Unit enemyUnit5;

    public Text dialogueText;

    public BattleHUD playerHUD;
    public BattleHUD[] enemyHUD = new BattleHUD[5];

    public Unit[] enemy = new Unit[5];

    public Animator[] magicAtk = new Animator[3];

    public Animator[] basicAtk = new Animator[3];

    public BattleState state;

    [SerializeField]
    public Color alive;

    [SerializeField]
    public Color dead;

    public AudioSource enemySound;

    public AudioSource magicSound;

    public AudioSource atkSound;

    public AudioSource healSound;

    // Start is called before the first frame update
    void Start()
    {

        if (PlayerPrefs.GetFloat("HP") != 400f)
        {
            newGame.SetActive(true);
        }
        else
            newGame.SetActive(false);

        enemyType[0] = enemyPrefab;
        enemyType[1] = enemyPrefab2;
        enemyType[2] = enemyPrefab3;

        enemyBattleStation[0] = EBS1;
        enemyBattleStation[1] = EBS2;
        enemyBattleStation[2] = EBS3;
        enemyBattleStation[3] = EBS4;
        enemyBattleStation[4] = EBS5;

        state = BattleState.START;
        StartCoroutine(SetupBattle());

    }

    IEnumerator SetupBattle()
    {

        //spawn = 2;
        spawn = Random.Range(2, 4);
        if (spawn == 2)
        {
            enemyHUD[2].gameObject.SetActive(false);
            EBS3.gameObject.SetActive(false);
            atkButton3.SetActive(false);
            enemyHUD[3].gameObject.SetActive(false);
            EBS4.gameObject.SetActive(false);
            atkButton4.SetActive(false);
            enemyHUD[4].gameObject.SetActive(false);
            EBS5.gameObject.SetActive(false);
            atkButton5.SetActive(false);
        }
        if (spawn == 3)
        {
            enemyHUD[3].gameObject.SetActive(false);
            EBS4.gameObject.SetActive(false);
            atkButton4.SetActive(false);
            enemyHUD[4].gameObject.SetActive(false);
            EBS5.gameObject.SetActive(false);
            atkButton5.SetActive(false);
        }
        if (spawn == 4)
        {
            enemyHUD[4].gameObject.SetActive(false);
            EBS5.gameObject.SetActive(false);
            atkButton5.SetActive(false);
        }

        GameObject playerGO = Instantiate(playerPrefab, playerBattleStation);
        playerUnit = playerGO.GetComponent<Unit>();

        if (PlayerPrefs.HasKey("HP"))
        {
            playerUnit.currentHP = PlayerPrefs.GetFloat("HP");
        }
        else
            playerUnit.currentHP = 400f;

        while (count < spawn)
        {
            type = Random.Range(0, 3);
            //type = 0;

            if (count == 0){
                GameObject enemyGO = Instantiate(enemyType[type], enemyBattleStation[count]);
                enemyUnit = enemyGO.GetComponent<Unit>();
                enemyHUD[count].SetHUD(enemyUnit);
                enemy[0] = enemyUnit;
            }

            if (count == 1)
            {
                GameObject enemyGO = Instantiate(enemyType[type], enemyBattleStation[count]);
                enemyUnit2 = enemyGO.GetComponent<Unit>();
                enemyHUD[count].SetHUD(enemyUnit2);
                enemy[1] = enemyUnit2;
            }

            if (count == 2)
            {
                GameObject enemyGO = Instantiate(enemyType[type], enemyBattleStation[count]);
                enemyUnit3 = enemyGO.GetComponent<Unit>();
                enemyHUD[count].SetHUD(enemyUnit3);
                enemy[2] = enemyUnit3;
            }

            if (count == 3)
            {
                GameObject enemyGO = Instantiate(enemyType[type], enemyBattleStation[count]);
                enemyUnit4 = enemyGO.GetComponent<Unit>();
                enemyHUD[count].SetHUD(enemyUnit4);
                enemy[3] = enemyUnit4;
            }

            if (count == 4)
            {
                GameObject enemyGO = Instantiate(enemyType[type], enemyBattleStation[count]);
                enemyUnit5 = enemyGO.GetComponent<Unit>();
                enemyHUD[count].SetHUD(enemyUnit5);
                enemy[4] = enemyUnit5;
            }

            count += 1;

        }

        dialogueText.text = spawn + " enemies approach!";

        playerHUD.SetHUD(playerUnit);

        yield return new WaitForSeconds(2f);

        state = BattleState.PLAYERTURN;
        PlayerTurn();
    }

    IEnumerator PlayerAttack(int target)
    {
        //Damages Enemy
        enemy[target].TakeDamage(playerUnit.atk, playerUnit.unitLevel, enemy[target].def);

        enemyHUD[target].SetHP(enemy[target].currentHP);
        basicAtk[target].Play("PlayerAttack");
        atkSound.Play();
        dialogueText.text = "The attack is successful!";

        yield return new WaitForSeconds(2f);

        if (enemy[0].currentHP <= 0 && enemy[1].currentHP <= 0 && enemy[2].currentHP <= 0 && enemy[3].currentHP <= 0 && enemy[4].currentHP <= 0)
        {
            //Ends Battles
            state = BattleState.WON;
            StartCoroutine(EndBattle());
        }
        else
        {
            //Enemy Turn
            state = BattleState.ENEMYTURN;
            StartCoroutine(EnemyTurn());
        }
    }

    IEnumerator EnemyTurn()
    {
        while (battleCount < spawn)
        {
            print(enemy[battleCount].currentHP);
            if (enemy[battleCount].currentHP <= 0)
            {
                dialogueText.text = (enemy[battleCount].unitName + " is struggling to get back up.");
                yield return new WaitForSeconds(2f);
                enemy[battleCount].currentHP += (enemy[battleCount].maxHP * .1f);
                enemyHUD[battleCount].SetHP(enemy[battleCount].currentHP);
                if (enemy[battleCount].currentHP > 0)
                {
                    dialogueText.text = enemy[battleCount].unitName + " got back up.";
                    yield return new WaitForSeconds(2f);
                }
                battleCount += 1;
            }
            else
            {
                dialogueText.text = enemy[battleCount].unitName + " attacks!";
                yield return new WaitForSeconds(1f);
                enemyAnim.Play("EnemyAttack");
                enemySound.Play();
                playerUnit.TakeDamage(enemy[battleCount].atk, enemy[battleCount].unitLevel, playerUnit.def);
                playerHUD.SetHP(playerUnit.currentHP);
                yield return new WaitForSeconds(1f);
                battleCount += 1;
            }

        }
        if (playerUnit.currentHP <= 0)
        {
            state = BattleState.LOST;
            StartCoroutine(EndBattle());
        }
        else
        {
            battleCount = 0;
            state = BattleState.PLAYERTURN;
            PlayerTurn();
        }


    }

    IEnumerator EndBattle()
    {
        if (state == BattleState.WON)
        {
            /*count = 0;
            while (count < spawn)
            {
                Destroy(enemy[count].gameObject);
                count += 1;
            }*/
            dialogueText.text = "Look out! More enemies are approaching!";
            //PersistentManager.Instance.Health = playerUnit.currentHP;
            yield return new WaitForSeconds(2f);
            SceneManager.LoadScene("Battle");
            PlayerPrefs.SetFloat("HP", playerUnit.currentHP);
            //state = BattleState.START;
            //StartCoroutine(SetupBattle());
        }  
        else if (state == BattleState.LOST)
        {
            dialogueText.text = "You were defeated.";
            yield return new WaitForSeconds(2f);
            PlayerPrefs.SetFloat("HP", 400f);
            Application.Quit();
        }
    }

    void PlayerTurn()
    {
        if (spawn >= 2)
        {
            atkButton1.SetActive(true);
            atkButton2.SetActive(true);
        }
        if (spawn >= 3)
        {
            atkButton3.SetActive(true);
        }
        if (spawn >= 4)
        {
            atkButton4.SetActive(true);
        }
        if (spawn >= 5)
        {
            atkButton5.SetActive(true);
        }
 
  
        healButton.SetActive(true);
        magicButton.SetActive(true);
        dialogueText.text = "Choose an action:";
    }

    IEnumerator PlayerHeal()
    {
        playerUnit.Heal(playerUnit.maxHP);

        playerHUD.SetHP(playerUnit.currentHP);
        dialogueText.text = "You feel refreshed.";
        healSound.Play();

        yield return new WaitForSeconds(2f);

        state = BattleState.ENEMYTURN;

        StartCoroutine(EnemyTurn());
    }

    IEnumerator PlayerMagic()
    {
        while (magCount < spawn)
        {
            enemy[magCount].MagicDamage(playerUnit.atk, playerUnit.unitLevel, enemy[magCount].def);
            enemyHUD[magCount].SetHP(enemy[magCount].currentHP);
            magicAtk[magCount].Play("MagicAttack");
            magicSound.Play();
            dialogueText.text = "The attack is successful!";

            magCount += 1;

        }

        magCount = 0;

        yield return new WaitForSeconds(2f);
        if (enemy[0].currentHP <= 0 && enemy[1].currentHP <= 0 && enemy[2].currentHP <= 0 && enemy[3].currentHP <= 0 && enemy[4].currentHP <= 0)
        {
            //Ends Battles
            state = BattleState.WON;
            StartCoroutine(EndBattle());
        }
        else
        {
            //Enemy Turn
            state = BattleState.ENEMYTURN;
            StartCoroutine(EnemyTurn());
        }
    }

    public void OnAttackButton0()
    {
        if (state != BattleState.PLAYERTURN)
            return;
        atkButton1.SetActive(false);
        atkButton2.SetActive(false);
        atkButton3.SetActive(false);
        atkButton4.SetActive(false);
        atkButton5.SetActive(false);
        healButton.SetActive(false);
        magicButton.SetActive(false);

        StartCoroutine(PlayerAttack(0));
    }
    public void OnAttackButton1()
    {
        if (state != BattleState.PLAYERTURN)
            return;
        atkButton1.SetActive(false);
        atkButton2.SetActive(false);
        atkButton3.SetActive(false);
        atkButton4.SetActive(false);
        atkButton5.SetActive(false);
        healButton.SetActive(false);
        magicButton.SetActive(false);

        StartCoroutine(PlayerAttack(1));
    }
    public void OnAttackButton2()
    {
        if (state != BattleState.PLAYERTURN)
            return;
        atkButton1.SetActive(false);
        atkButton2.SetActive(false);
        atkButton3.SetActive(false);
        atkButton4.SetActive(false);
        atkButton5.SetActive(false);
        healButton.SetActive(false);
        magicButton.SetActive(false);

        StartCoroutine(PlayerAttack(2));
    }
    public void OnAttackButton3()
    {
        if (state != BattleState.PLAYERTURN)
            return;
        atkButton1.SetActive(false);
        atkButton2.SetActive(false);
        atkButton3.SetActive(false);
        atkButton4.SetActive(false);
        atkButton5.SetActive(false);
        healButton.SetActive(false);
        magicButton.SetActive(false);

        StartCoroutine(PlayerAttack(3));
    }
    public void OnAttackButton4()
    {
        if (state != BattleState.PLAYERTURN)
            return;
        atkButton1.SetActive(false);
        atkButton2.SetActive(false);
        atkButton3.SetActive(false);
        atkButton4.SetActive(false);
        atkButton5.SetActive(false);
        healButton.SetActive(false);
        magicButton.SetActive(false);

        StartCoroutine(PlayerAttack(4));
    }

    public void OnHealButton()
    {
        if (state != BattleState.PLAYERTURN)
            return;
        atkButton1.SetActive(false);
        atkButton2.SetActive(false);
        atkButton3.SetActive(false);
        atkButton4.SetActive(false);
        atkButton5.SetActive(false);
        healButton.SetActive(false);
        magicButton.SetActive(false);

        StartCoroutine(PlayerHeal());
    }
    
    public void OnMagicButton()
    {
        if (state != BattleState.PLAYERTURN)
            return;
        atkButton1.SetActive(false);
        atkButton2.SetActive(false);
        atkButton3.SetActive(false);
        atkButton4.SetActive(false);
        atkButton5.SetActive(false);
        healButton.SetActive(false);
        magicButton.SetActive(false);

        StartCoroutine(PlayerMagic());
    }

    public void NewGame()
    {
        PlayerPrefs.SetFloat("HP", 400f);
        SceneManager.LoadScene("Battle");
    }

    public void QuitGame()
    {
        Application.Quit();
    }
}
