﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
public class SceneManagerScript : MonoBehaviour
{

    public float valueHP;

    // Start is called before the first frame update
    void Start()
    {
        valueHP = PersistentManager.Instance.Health;
    }

    public void GoToFirstScene()
    {
        SceneManager.LoadScene("Battle");
    }
}
